import { Component , OnInit    } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export   class LoginComponent implements OnInit {
 
  fieldtoggleShowPassword: boolean = false;
  constructor(      private router: Router  ) {
     
   }
  ngOnInit(): void {
  }

  
 
  
 
  toggleShowPassword() {
    this.fieldtoggleShowPassword = !this.fieldtoggleShowPassword;
  }
  Submit(){
    this.router.navigate(['companies']);
  }

}
