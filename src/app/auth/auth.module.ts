import { NgModule } from '@angular/core';
import { AsyncPipe, CommonModule } from '@angular/common';
import { FeatherModule } from 'angular-feather';
import { allIcons } from 'angular-feather/icons';
import { DemoFlexyModule } from '../demo-flexy-module';
import { TranslateModule } from '@ngx-translate/core';
  import {LoginComponent} from './login/login.component';
@NgModule({
  declarations: [  LoginComponent ],
  imports: [
    CommonModule,
    FeatherModule.pick(allIcons),
    DemoFlexyModule,
    TranslateModule,
  ],
})
export class AuthModule { }
