import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {HashLocationStrategy, Location, LocationStrategy} from '@angular/common';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  location: Location;
 
  title = 'flexy-angular';
  constructor(private translate: TranslateService) {
    translate.setDefaultLang('en');
    translate.use('en');
  
  }

  switchLanguage(language: string) {
    this.translate.use(language);
  }
  
}
