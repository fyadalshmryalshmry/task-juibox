import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';


@NgModule({
  declarations: [
  ],
  exports: [],
  imports: [TranslateModule, MatCardModule,
    CommonModule, FormsModule, ReactiveFormsModule
  ]
})
export class SharedModule { }
