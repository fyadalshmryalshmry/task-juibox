import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent implements OnInit {
  list_country: any[] = ['Damascus','Alzabadani','Daraa','Lattakia','Der Al-Zour','Aleepo','Raqqa','Hammah','As-Suwayda' ];
  companies: any[] = [1,2,3,4,5,6,7,8,9,10,11,1,1,1,1,1,1,1,1,1,1,1];
  trips: any[] = [1,2,3,4,5,6,7,8,9,10,11];
  active_trip: any =  0
  isLoading = true;
  constructor( ) { }
  ngOnInit() {
     
  }
  click_trip(trip: any) {
    this.active_trip = trip;
    
  }
}
