import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompaniesComponent } from './companies.component';
import { RouterModule, Routes } from '@angular/router';
import { DemoFlexyModule } from 'src/app/demo-flexy-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { NgApexchartsModule } from 'ng-apexcharts';
import { FeatherModule } from 'angular-feather';
import { allIcons } from 'angular-feather/icons';
 import {MatDialogModule} from '@angular/material/dialog';
import {MatChipsModule} from '@angular/material/chips';
// import { ContinueBookSeatComponent } from './continue-book-seat/continue-book-seat.component';
import { TranslateModule } from '@ngx-translate/core';
// import { InfiniteScrollModule } from 'angular2-infinite-scroll';

const routes: Routes = [
  {
    path: '',
    component: CompaniesComponent,
  },
 
];

@NgModule({
  declarations: [
    CompaniesComponent,
   ],
  imports: [
    CommonModule,TranslateModule,
    DemoFlexyModule,
    FormsModule,
    RouterModule.forChild(routes),
    FeatherModule.pick(allIcons),
    ReactiveFormsModule,
    MatDialogModule,
    MatChipsModule
  ]
})
export class CompaniesModule { }
