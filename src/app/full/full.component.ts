import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { NavigationStart, Router } from '@angular/router';
 
import { TranslateService } from '@ngx-translate/core';


interface sidebarMenu {
  link: string;
  icon: string;
  menu: string;
  icon_active:string
}

@Component({
  selector: 'app-full',
  templateUrl: './full.component.html',
  styleUrls: ['./full.component.scss']
})
export class FullComponent {

  search: boolean = false;
  base: string = 'Companies';
  icone: string = "assets/images/icon/Users-color.svg";
  splitVal: any;
 
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );




  constructor(  private breakpointObserver: BreakpointObserver, private router: Router,private translate: TranslateService) {
    
      translate.setDefaultLang('en');
  }

  ngOnInit() {
    
  }
   
   
  routerActive: string = "activelink";
  sidebarMenu: sidebarMenu[] = [
    {
      link: "/dashboard",
      icon: "assets/images/icon/dashboard-not.png",
      icon_active: "assets/images/icon/dash-icon.png",
      menu: "Dashboard",
    },
    {
      link: "/companies",
      icon: "assets/images/icon/3-Friends.png",
      icon_active: "assets/images/icon/3-Friends.png",
      menu: "Companies",
    },
    {
      link: "/bus-layout/bus-layout",
      icon: "assets/images/icon/ri_bus-fill.png",
      icon_active: "assets/images/icon/dash-icon.png",
      menu: "Bus & Layouts",
    },
    {
      link: "/notification",
      icon: "assets/images/icon/Notification.png",
      icon_active: "assets/images/icon/Notification.png",
      menu: 'Notifications'   },
    {
      link: "/profile/profile",
      icon: "assets/images/icon/Profile.png",
      icon_active: "assets/images/icon/dash-icon.png",
      menu: "Profile",
    },
  ]
 
   
   
  Login(){
    this.router.navigate(['login']);
  }
   
}
