 

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

 import { FullComponent } from './full/full.component';
 import { LoginComponent } from './auth/login/login.component';

const routes: Routes = [
  {
    
    path: '',
    redirectTo: 'companies',
    pathMatch: 'full'
  },
  {
    path: "",
    component: FullComponent,
    children: [
      
      {
        path: 'companies',
        loadChildren: () =>
          import('./modules/companies/companies.module').then((m) => m.CompaniesModule),
      },   
         
     ]
  },
  { path: "login", component: LoginComponent },  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }